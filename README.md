This is an example of a Quality Lab Web automation project.



We're using Selenide, Cucumber and Java.



Selenide is a framework made in Estonia, on the base of Selenium. It gives a great boilerplate-free code and is also really easy to write.



Cucumber is used mainly to fit our customers needs. It gives good overview what tests exist and also generates nice HTML report.