@Demo
Feature: Tieto search engine
  Scenario: Search Geek-Off 2015 from Tieto.ee search engine
    Given I open Tieto Estonia homepage
    Then I should be on the Tieto Estonia homepage
    When I click on the search icon on the right
    Then I should see modal view search engine
    When I search for "Geek-Off 2015"
    Then search engine input should have "Geek-Off 2015"
    And there should be 1 result
    When I open the result
    Then I should see Geek out camp mascot
