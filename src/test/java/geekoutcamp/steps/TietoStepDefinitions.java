package geekoutcamp.steps;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import geekoutcamp.pages.HomePage;
import geekoutcamp.pages.NewsPage;
import geekoutcamp.pages.SearchPage;

import static com.codeborne.selenide.Selenide.open;

public class TietoStepDefinitions {

  Scenario scenario;

  @Before
  public void setup(Scenario scenario) {
    this.scenario = scenario;
  }

  @Given("^I open Tieto Estonia homepage$")
  public void i_open_Tieto_Estonia_homepage() {
    open("http://www.tieto.ee/");
  }

  @Then("^I should be on the Tieto Estonia homepage$")
  public void I_should_be_on_the_Tieto_Estonia_homepage(){
    HomePage home = new HomePage(scenario);
    home.shouldSeeSearchIcon();
  }

  @When("^I click on the search icon on the right$")
  public void I_click_on_the_search_icon_on_the_right() {
    HomePage home = new HomePage(scenario);
    home.clickOnTheSearch();
  }

  @Then("^I should see modal view search engine$")
  public void I_should_see_modal_view_search_engine() {
    HomePage home = new HomePage(scenario);
    home.shouldSeeSearchModal();
  }

  @When("^I search for \"([^\"]*)\"$")
  public void I_search_for(String text) {
    HomePage home = new HomePage(scenario);
    home.search(text);
  }

  @Then("^search engine input should have \"([^\"]*)\"$")
  public void search_engine_input_should_have(String text) {
    SearchPage search = new SearchPage(scenario);
    search.shouldHaveValue(text);
  }

  @And("^there should be (\\d+) result$")
  public void there_should_be_result(int count) {
    SearchPage search = new SearchPage(scenario);
    search.resultsCountShouldBe(count);
  }

  @When("^I open the result$")
  public void I_open_the_result() {
    SearchPage search = new SearchPage(scenario);
    search.selectResult();
  }

  @Then("^I should see Geek out camp mascot$")
  public void I_should_see_Geek_out_camp_mascot() {
    NewsPage news = new NewsPage(scenario);
    news.shouldSeeGeek();
  }
}
