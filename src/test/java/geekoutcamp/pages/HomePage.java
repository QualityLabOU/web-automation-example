package geekoutcamp.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.Scenario;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HomePage {

  Scenario scenario;

  SelenideElement searchIcon = $(By.className("search-block-link"));
  SelenideElement searchModal = $(By.id("tieto-lightbox-content-wrapper"));
  SelenideElement searchInputField = $$(By.className("form-text")).filter(Condition.visible).get(0);
  SelenideElement searchForm = $$(By.id("search-block-form")).filter(Condition.visible).get(0);

  public HomePage(Scenario scenario){
    this.scenario = scenario;
  }

  public void shouldSeeSearchIcon() {
    searchIcon.shouldBe(Condition.visible);
  }

  public void clickOnTheSearch() {
    searchIcon.click();
  }

  public void shouldSeeSearchModal() {
    searchModal.shouldBe(Condition.visible);
  }

  public void search(String text) {
    setSearchInputValue(text);
    searchForm.submit();
  }

  private void setSearchInputValue(String text) {
    searchInputField.setValue(text);
    if (!searchInputField.val().equals(text)){
      throw new AssertionError("Something went wrong with writing to the search input");
    }
  }
}
