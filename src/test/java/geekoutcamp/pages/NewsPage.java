package geekoutcamp.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.Scenario;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class NewsPage {

  Scenario scenario;

  public NewsPage(Scenario scenario){
    this.scenario = scenario;
  }

  public void shouldSeeGeek() {
    ElementsCollection images = $$(By.tagName("img"));
    for (SelenideElement img : images){
      if (img.getAttribute("alt").equals("Geek")){
        return;
      }
    }
    throw new AssertionError("Geek is not visible");
  }
}
