package geekoutcamp.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.Scenario;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage {

  Scenario scenario;

  SelenideElement searchInput = $(By.id("edit-keys"));
  ElementsCollection results = $$(By.tagName("article"));

  public SearchPage(Scenario scenario){
    this.scenario = scenario;
  }


  public void shouldHaveValue(String text) {
   if (!searchInput.val().equals(text)){
     throw new AssertionError("Search engine input do not have value: "+text+" Actual value is: "+searchInput.val());
   }
  }

  public void resultsCountShouldBe(int count) {
    if (results.size() != count){
      throw new AssertionError("Results count should be: "+count+ " but actual was: "+results.size());
    }
  }

  public void selectResult() {
    results.get(0).find(By.tagName("a")).click();  }
}
