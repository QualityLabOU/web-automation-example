package geekoutcamp;

import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"pretty", "junit:reports/junit/junit.xml", "html:reports/html", "json:reports/cucumber-report.json"},
        tags = {"@Demo"}

)
public class RunTests {

  @BeforeClass
  public static void setup() {
    System.out.print("Test started");
  }

  @AfterClass
  public static void teardown() {
    System.out.print("Test ended");
    WebDriverRunner.closeWebDriver();
  }

}
